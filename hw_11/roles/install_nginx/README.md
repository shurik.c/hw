Role Name
=========

Установка nginx на сервера с CentOs/7 или Ubuntu 16 на борту

Requirements
------------

CentOs/7 или Ubuntu 16, systemd, (yum\apt)

Role Variables
--------------

Переменные определены в `defaults` директории

Example Playbook
----------------

Добавляем в playbook
```
  roles:
  - { role: install_nginx }
``` 