# Домашнее задание

## Первые шаги с Ansible
### Подготовить стенд на Vagrant как минимум с одним сервером.

Подготовлен [Vagrantfile](Vagrantfile) с двумя машинами nginx-vm-1 и nginx-vm-2 с CentOs и Ubuntu на борту

 ### На этом сервере используя Ansible необходимо развернуть nginx со следующими условиями:

 
#### - необходимо использовать модуль yum/apt

Для сервера с CentOs используем yum:
```
  - name: NGINX | Install Nginx
    yum:
    ...
  when: ansible_os_family == "RedHat" 
```
Для сервера с Ubuntu используем apt:
```
  - name: NGINX | Install Nginx
    apt:
    ...
  when: ansible_os_family == "Debian"  
```

#### - конфигурационные файлы должны быть взяты из шаблона jinja2 с перемененными

Используются шаблоны для файлов [nginx.conf.j2](roles/install_nginx/templates/RedHat/nginx.conf.j2) и [index.html.j2](roles/install_nginx/templates/index.html.j2)

#### - после установки nginx должен быть в режиме enabled в systemd
#### - должен быть использован notify для старта nginx после установки

После установки пакета nginx, запускает [handler](roles/install_nginx/handlers/main.yml), который перезапускает сервис nginx и переводит в режим enabled:

```
- name: restart nginx
  systemd:
    name: nginx
    state: restarted
    enabled: yes
```

#### - сайт должен слушать на нестандартном порту - 8080, для этого использовать переменные в Ansible

Добавлена переменная в файле [main.yml](roles/install_nginx/defaults/main.yml):
```
nginx_port : 8080
```

используется в шаблонах:
```
server {
        listen {{ nginx_port }} default_server;
        listen [::]:{{ nginx_port }} default_server;
        ...
```

#### * Сделать все это с использованием Ansible роли

[playbook](playbook.yml) запускается с помощью роли:
```
  roles:
  - { role: install_nginx }
```
