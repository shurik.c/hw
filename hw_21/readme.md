Домашнее задание
настраиваем split-dns
взять стенд https://github.com/erlong15/vagrant-bind
добавить еще один сервер client2
завести в зоне dns.lab 
имена
web1 - смотрит на клиент1
web2 смотрит на клиент2

создаём записи:
```
web1            IN      A       192.168.50.15
web2            IN      A       192.168.50.16
```

#### завести еще одну зону newdns.lab
#### завести в ней запись
#### www - смотрит на обоих клиентов
добавлена зона [newdns.lab](provisioning/named.newdns.lab)

c записями записи:
```
www             IN      A       192.168.50.15
www             IN      A       192.168.50.16
```
#### настроить split-dns

#### клиент1 - видит обе зоны, но в зоне dns.lab только web1
split-dns настраивается с помощью технологий view и acl:
```
...
acl web1 { 192.168.50.15; };
...
view "web1" {
    match-clients { web1; };

    recursion yes;

    // lab's zone
    zone "dns.lab" {
        type master;
        allow-transfer { key "zonetransfer.key"; };
        file "/etc/named/named.dns.web1.lab";
    };
...
```
для обоих клиетов созданы свои зоны видимости.

для client создана отдельная зона [dns.lab](provisioning/named.dns.web1.lab)

<details><summary>nslookup</summary>
<p>

```
[root@client ~]# nslookup www.newdns.lab
Server:		192.168.50.10
Address:	192.168.50.10#53

Name:	www.newdns.lab
Address: 192.168.50.15
Name:	www.newdns.lab
Address: 192.168.50.16

[root@client ~]# nslookup web1.dns.lab
Server:		192.168.50.10
Address:	192.168.50.10#53

Name:	web1.dns.lab
Address: 192.168.50.15

[root@client ~]# nslookup web2.dns.lab
Server:		192.168.50.10
Address:	192.168.50.10#53

** server can't find web2.dns.lab: NXDOMAIN
```
</p>
</details>


#### клиент2 видит только dns.lab
<details><summary>dig</summary>
<p>

```
[root@client2 ~]# dig www.newdns.lab 192.168.50.11 +short
[root@client2 ~]# dig web1.dns.lab 192.168.50.11 +short
192.168.50.15
[root@client2 ~]# dig web2.dns.lab 192.168.50.11 +short
192.168.50.16
```
</p>
</details>

#### *) настроить все без выключения selinux
#### ddns тоже должен работать без выключения selinux
для работы bind c selinux все файлы конфигурации внутри каталога /etc/named должны иметь тип named_zone_t  

добавим в плейбук 'setype=named_zone_t' и проверяем:

```
[vagrant@client ~]$ nsupdate -k /etc/named.zonetransfer.key
> server 192.168.50.10
> zone ddns.lab
> update add www.ddns.lab. 60 A 192.168.50.15
> send
[vagrant@client ~]$ dig www.ddns.lab 192.168.50.11 +short
192.168.50.15
```