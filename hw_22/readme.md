# Домашнее задание
##Сценарии iptables
#### 1) реализовать knocking port
##### - centralRouter может попасть на ssh inetrRouter через knock скрипт

Реализуем на iptables:
сначала будем стучаться на порт 7788 и попадать в сет SSH0
```
-A TRAFFIC -s 192.168.255.2/32 -p tcp -m state --state NEW -m tcp --dport 7788 -m recent --set --name SSH0 --mask 255.255.255.255 --rsource -j DROP
```
затем 1 раз запуститм пинг и попадём в фильтр SSH-INPUT1
```
-A TRAFFIC -s 192.168.255.2/32 -p icmp -m state --state NEW -m icmp --icmp-type 8 -m recent --rcheck --seconds 15 --name SSH0 --mask 255.255.255.255 --rsource -j SSH-INPUT1
```
при следующем проходе попадаем в сет SSH1 и стучимся на порт 7887 и попадём в фильтр SSH-INPUT2
```
-A SSH-INPUT1 -m recent --set --name SSH1 --mask 255.255.255.255 --rsource -j DROP
...
-A TRAFFIC -s 192.168.255.2/32 -p tcp -m state --state NEW -m tcp --dport 7887 -m recent --rcheck --seconds 15 --name SSH1 --mask 255.255.255.255 --rsource -j SSH-INPUT2
```
попадаем в сет SSH2 после чего и получаем доступ по ssh на 60 секунд.
```
-A SSH-INPUT1 -m recent --set --name SSH1 --mask 255.255.255.255 --rsource -j DROP
...
-A TRAFFIC -p tcp -m state --state NEW -m tcp --dport 22 -m recent --rcheck --seconds 60 --name SSH2 --mask 255.255.255.255 --rsource -j ACCEPT
```
<details><summary>проверяем:</summary>
<p>    

```
[root@centralRouter ~]# ./kn_script.sh
Starting Nmap 6.40 ( http://nmap.org ) at 2019-04-02 14:49 UTC
Warning: 192.168.255.1 giving up on port because retransmission cap hit (0).
Nmap scan report for 192.168.255.1
Host is up (0.00042s latency).
PORT     STATE    SERVICE
7788/tcp filtered unknown
MAC Address: 08:00:27:49:0C:63 (Cadmus Computer Systems)

Nmap done: 1 IP address (1 host up) scanned in 0.14 seconds
PING 192.168.255.1 (192.168.255.1) 56(84) bytes of data.
--- 192.168.255.1 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms


Starting Nmap 6.40 ( http://nmap.org ) at 2019-04-02 14:49 UTC
Warning: 192.168.255.1 giving up on port because retransmission cap hit (0).
Nmap scan report for 192.168.255.1
Host is up (0.00049s latency).
PORT     STATE    SERVICE
7887/tcp filtered unknown
MAC Address: 08:00:27:49:0C:63 (Cadmus Computer Systems)

Nmap done: 1 IP address (1 host up) scanned in 0.15 seconds
The authenticity of host '192.168.255.1 (192.168.255.1)' can't be established.
ECDSA key fingerprint is SHA256:2ZiC5hZ9M2p5h3PuuYj4hg5IePJ+ZW32ICoXbVKPkNM.
ECDSA key fingerprint is MD5:11:4b:92:27:74:b0:c6:82:21:ff:57:e9:94:b0:20:e8.
Are you sure you want to continue connecting (yes/no)?
```
</p>
</details>

пример в материалах
##### 1) добавить inetRouter2, который виден(маршрутизируется) с хоста
##### 2) запустить nginx на centralServer
##### 3) пробросить 80й порт на inetRouter2 8080
в этот раз используем firewalld:
включем его настроим нат и пробросим порт:
```
systemctl start firewalld.service
systemctl enable firewalld.service
firewall-cmd --permanent --zone=public --add-masquerade
firewall-cmd --permanent --zone=public --add-forward-port=port=8080:proto=tcp:toport=80:toaddr=192.168.0.2
```
проверяем:
```
~ > curl 192.168.11.151:8080
Nginx!
```
##### 4) дефолт в инет оставить через inetRouter
на centralRouter добавим маршрут до inetRouter2 `nmcli c mod "System eth1" ipv4.routes "192.168.11.151 192.168.255.5"`