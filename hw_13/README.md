# Домашнее задание
## Настраиваем Backup
## Настроить стенд Vagrant с двумя виртуальными машинами server и client.

### Настроить политику бэкапа директории /etc с клиента:
### 1) Полный бэкап - раз в день
### 2) Инкрементальный - каждые 10 минут
### 3) Дифференциальный - каждые 30 минут

## Запустить систему на два часа. Для сдачи ДЗ приложить list jobs, list files jobid=\<id>
### и сами конфиги bacula-*

### Настроить доп. Опции - сжатие, шифрование, дедупликация

Написаны роли для установки и настройки [mariadb](roles/mysql_bacula) и [bacula](roles/bacula)

Настроен шаблон для бэкапа /etc на всех клиентах

```
Schedule {
  Name = "DailyCycle"
  Run = Full daily at 00:05
  Run = Differential hourly at 00:05
  Run = Differential hourly at 00:35
  Run = Incremental hourly at 00:05
  Run = Incremental hourly at 00:15
  Run = Incremental hourly at 00:25
  Run = Incremental hourly at 00:35
  Run = Incremental hourly at 00:45
  Run = Incremental hourly at 00:55
}
```
Cжатие настраивается на директоре:
```
FileSet {
  Name = "ETC_set"
  Include {
    Options {
      ...
      compression = GZIP
      ...
```
Для настройки шифрования на клиентах неоходимо сгенерировать ключи и добавить в конфигурацию:
```
FileDaemon {
  ... 
  PKI Signatures = Yes
  PKI Encryption = Yes
  PKI Keypair = "/etc/bacula/fd-client.pem"
  PKI Master Key = "/etc/bacula/master.cert"
```
Дедупликация настравается, изменением уовнем бэкапа на `base` и добавлением в секцию Job полей:
```
Job {
  ...
  Level = base
  Accurate = yes
  Spool Attributes = yes
  ...
```