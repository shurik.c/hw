+-------+-----------------------+---------------------+------+-------+----------+------------+-----------+
| JobId | Name                  | StartTime           | Type | Level | JobFiles | JobBytes   | JobStatus |
+-------+-----------------------+---------------------+------+-------+----------+------------+-----------+
|     1 | back-host_etc_backup  | 2019-01-28 07:14:08 | B    | B     |    2,398 | 11,385,312 | T         |
|     2 | bacula-srv_etc_backup | 2019-01-28 07:15:02 | B    | B     |    2,407 | 11,388,752 | T         |
|     3 | back-host_etc_backup  | 2019-01-28 07:15:16 | B    | F     |    2,398 | 11,385,312 | T         |
|     4 | bacula-srv_etc_backup | 2019-01-28 07:16:00 | B    | F     |    2,407 | 11,388,752 | T         |
|     5 | back-host_etc_backup  | 2019-01-28 07:25:00 | B    | I     |        0 |          0 | T         |
|     6 | bacula-srv_etc_backup | 2019-01-28 07:25:02 | B    | I     |        0 |          0 | T         |
|     7 | back-host_etc_backup  | 2019-01-28 07:35:01 | B    | D     |        0 |          0 | T         |
|     8 | back-host_etc_backup  | 2019-01-28 07:35:04 | B    | I     |        0 |          0 | T         |
|     9 | bacula-srv_etc_backup | 2019-01-28 07:35:08 | B    | D     |        0 |          0 | T         |
|    10 | bacula-srv_etc_backup | 2019-01-28 07:35:14 | B    | I     |        0 |          0 | T         |
|    11 | back-host_etc_backup  | 2019-01-28 07:45:00 | B    | I     |        0 |          0 | T         |
|    12 | bacula-srv_etc_backup | 2019-01-28 07:45:02 | B    | I     |        0 |          0 | T         |
|    13 | back-host_etc_backup  | 2019-01-28 07:55:00 | B    | I     |        0 |          0 | T         |
|    14 | bacula-srv_etc_backup | 2019-01-28 07:55:02 | B    | I     |        0 |          0 | T         |
|    15 | back-host_etc_backup  | 2019-01-28 08:05:01 | B    | D     |        2 |        672 | T         |
|    16 | back-host_etc_backup  | 2019-01-28 08:05:03 | B    | I     |        0 |          0 | T         |
|    17 | bacula-srv_etc_backup | 2019-01-28 08:05:04 | B    | D     |        2 |        672 | T         |
|    18 | bacula-srv_etc_backup | 2019-01-28 08:05:06 | B    | I     |        0 |          0 | T         |
|    19 | back-host_etc_backup  | 2019-01-28 08:15:00 | B    | I     |        6 |      4,496 | T         |
|    20 | bacula-srv_etc_backup | 2019-01-28 08:15:04 | B    | I     |        2 |        672 | T         |
|    21 | back-host_etc_backup  | 2019-01-28 08:25:00 | B    | I     |        0 |          0 | T         |
|    22 | bacula-srv_etc_backup | 2019-01-28 08:25:02 | B    | I     |        2 |          0 | T         |
|    23 | back-host_etc_backup  | 2019-01-28 08:35:00 | B    | D     |        7 |      4,496 | T         |
|    24 | back-host_etc_backup  | 2019-01-28 08:35:02 | B    | I     |        0 |          0 | T         |
|    25 | bacula-srv_etc_backup | 2019-01-28 08:35:03 | B    | D     |        2 |        672 | T         |
|    26 | bacula-srv_etc_backup | 2019-01-28 08:35:04 | B    | I     |        0 |          0 | T         |
|    27 | back-host_etc_backup  | 2019-01-28 08:45:00 | B    | I     |        0 |          0 | T         |
|    28 | bacula-srv_etc_backup | 2019-01-28 08:45:02 | B    | I     |        0 |          0 | T         |
|    29 | back-host_etc_backup  | 2019-01-28 08:55:00 | B    | I     |        0 |          0 | T         |
|    30 | bacula-srv_etc_backup | 2019-01-28 08:55:02 | B    | I     |        0 |          0 | T         |
|    31 | back-host_etc_backup  | 2019-01-28 09:05:00 | B    | D     |        7 |      4,496 | T         |
|    32 | back-host_etc_backup  | 2019-01-28 09:05:02 | B    | I     |        0 |          0 | T         |
|    33 | bacula-srv_etc_backup | 2019-01-28 09:05:04 | B    | D     |        4 |      2,720 | T         |
|    34 | bacula-srv_etc_backup | 2019-01-28 09:05:05 | B    | I     |        0 |          0 | T         |
+-------+-----------------------+---------------------+------+-------+----------+------------+-----------+