mysql_bacula
=========

Роль для настройки Mariadb для bacula

Requirements
------------

CentOs 7

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: mysql_bacula }
