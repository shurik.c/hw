Bacula
=========

Установка и настройка Bacula

Requirements
------------

CentOs 7. Настроенная бд для работы с bacula

Role Variables
--------------

Переменные указаны в [defaults](defaults/main.yml) 


Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: bacula }

подробнее о [шифровании](https://www.bacula.org/5.2.x-manuals/en/main/main/Data_Encryption.html), 
[сжатии](https://www.bacula.org/7.0.x-manuals/en/main/Basic_Volume_Management.html), 
[дедупликации](https://www.casp.ru/bacula-deduplication/)