
# Домашнее задание
## OSPF
#### Поднять три виртуалки
Написан [Vagrantfile](Vagrantfile)
#### Объединить их разными vlan
  Создаём vlan интерфейсы:
```
nmcli c add type vlan ifname eth1-vlan12 dev eth1 id 12 ip4 192.168.0.1/30
nmcli c add type vlan ifname eth1-vlan13 dev eth1 id 13 ip4 192.168.0.10/30
```
и даём второй адрес на loopback-интерфейс:
```
ip addr add 192.168.1.1/24 dev lo
``` 
###### 1. Поднять OSPF между машинами на базе Quagga
Устанавливаеем пакет Quagga `yum install -y quagga` , копируем пример конфигурации и меняем права для файла:
```
cp /usr/share/doc/quagga-*/ospfd.conf.sample /etc/quagga/ospfd.conf
chown quagga:quagga /etc/quagga/ospfd.conf
```
настраиваем selinux: `setsebool -P zebra_write_config 1` и запускаем сервисы *zebra и ospfd*.
Поднимаем OSPF  на всех роутерах:
```
[root@Router1 ~]# vtysh 

Hello, this is Quagga (version 0.99.22.4).
Copyright 1996-2005 Kunihiro Ishiguro, et al.

Router1# conf t
Router1(config)# router ospf
Router1(config-router)# router-id 1.1.1.1
Router1(config-router)# network 192.168.0.0/24 area 0
Router1(config-router)# network 192.168.1.0/24 area 1
Router1(config-router)# do wr
Building Configuration...
Configuration saved to /etc/quagga/zebra.conf
Configuration saved to /etc/quagga/ospfd.conf
[OK]
```
Проверяем, что всё заработало:
```
Router3# sh ip ospf  neighbor  

    Neighbor ID Pri State           Dead Time Address         Interface            RXmtL RqstL DBsmL
1.1.1.1           1 Full/DR           31.262s 192.168.0.10    eth1-vlan13:192.168.0.9     0     0     0
2.2.2.2           1 Full/DR           33.215s 192.168.0.5     eth1-vlan23:192.168.0.6     0     0     0
```

###### 2. Изобразить ассиметричный роутинг
Увеличиваем значение bandwidth на интерфейсе eth1-vlan13 роутера Router1:
```
Router1# interface eth1-vlan13
Router1(config-if)# bandwidth
Router1(config-if)# do sh ip ospf 
interface  eth1-vlan13
...
eth1-vlan13 is up
  ifindex 6, MTU 1500 bytes, BW 1000 Kbit <UP,BROADCAST,RUNNING,MULTICAST>
...
  Router ID 1.1.1.1, Network Type BROADCAST, Cost: 100
...
```
цена маршрута возросла до 100    
Проверяем маршрутизацию:
на Router1 теперь все маршруты идут через Router2: 
```
Router1(config-if)# do sh ip route
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, A - Babel,
       > - selected route, * - FIB route

K>* 0.0.0.0/0 via 10.0.2.2, eth0
C>* 10.0.2.0/24 is directly connected, eth0
C>* 127.0.0.0/8 is directly connected, lo
O   192.168.0.0/30 [110/10] is directly connected, eth1-vlan12, 01:16:56
C>* 192.168.0.0/30 is directly connected, eth1-vlan12
O>* 192.168.0.4/30 [110/20] via 192.168.0.2, eth1-vlan12, 00:08:54
O   192.168.0.8/30 [110/30] via 192.168.0.2, eth1-vlan12, 00:08:54
C>* 192.168.0.8/30 is directly connected, eth1-vlan13
C>* 192.168.1.0/24 is directly connected, lo
O>* 192.168.1.1/32 [110/10] is directly connected, lo, 01:28:44
O>* 192.168.2.1/32 [110/20] via 192.168.0.2, eth1-vlan12, 01:16:49
O>* 192.168.3.1/32 [110/30] via 192.168.0.2, eth1-vlan12, 00:08:54
```
на Router3 маршруты остались такими же
```
Router3# sh ip route  
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, A - Babel,
       > - selected route, * - FIB route

K>* 0.0.0.0/0 via 10.0.2.2, eth0
C>* 10.0.2.0/24 is directly connected, eth0
C>* 127.0.0.0/8 is directly connected, lo
O>* 192.168.0.0/30 [110/20] via 192.168.0.10, eth1-vlan13, 01:20:45
  *                         via 192.168.0.5, eth1-vlan23, 01:20:45
O   192.168.0.4/30 [110/10] is directly connected, eth1-vlan23, 01:21:21
C>* 192.168.0.4/30 is directly connected, eth1-vlan23
O   192.168.0.8/30 [110/10] is directly connected, eth1-vlan13, 01:27:00
C>* 192.168.0.8/30 is directly connected, eth1-vlan13
O>* 192.168.1.1/32 [110/20] via 192.168.0.10, eth1-vlan13, 01:26:52
O>* 192.168.2.1/32 [110/20] via 192.168.0.5, eth1-vlan23, 01:21:10
C>* 192.168.3.0/24 is directly connected, lo
O>* 192.168.3.1/32 [110/10] is directly connected, lo, 01:26:56
```
###### 3. Сделать один из линков "дорогим", но что бы при этом роутинг был симметричным
Выравниваем цену маршрута на Router3 и проверяем:
```
Router3(config)# interface eth1-vlan13
Router3(config-if)# bandwidth 1000
Router3(config-if)# do sh ip route  
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, A - Babel,
       > - selected route, * - FIB route

K>* 0.0.0.0/0 via 10.0.2.2, eth0
C>* 10.0.2.0/24 is directly connected, eth0
C>* 127.0.0.0/8 is directly connected, lo
O>* 192.168.0.0/30 [110/20] via 192.168.0.5, eth1-vlan23, 00:00:06
O   192.168.0.4/30 [110/10] is directly connected, eth1-vlan23, 01:29:20
C>* 192.168.0.4/30 is directly connected, eth1-vlan23
O   192.168.0.8/30 [110/100] is directly connected, eth1-vlan13, 00:00:06
C>* 192.168.0.8/30 is directly connected, eth1-vlan13
O>* 192.168.1.1/32 [110/30] via 192.168.0.5, eth1-vlan23, 00:00:06
O>* 192.168.2.1/32 [110/20] via 192.168.0.5, eth1-vlan23, 01:29:09
C>* 192.168.3.0/24 is directly connected, lo
O>* 192.168.3.1/32 [110/10] is directly connected, lo, 01:34:55
```
Присылаем 
<details><summary> Вывод ip a</summary>
<p>

#### Router1

```
lo               UNKNOWN        127.0.0.1/8 192.168.1.1/24 ::1/128 
eth0             UP             10.0.2.15/24 fe80::5054:ff:fe26:1060/64 
eth1             UP             
eth1-vlan13@eth1 UP             192.168.0.10/30 fe80::55c8:670f:923b:962f/64 
eth1-vlan12@eth1 UP             192.168.0.1/30 fe80::1ee0:6d31:c35d:9cc/64 
```
#### Router2
```
lo               UNKNOWN        127.0.0.1/8 192.168.1.1/24 ::1/128 
eth0             UP             10.0.2.15/24 fe80::5054:ff:fe26:1060/64 
eth1             UP             
eth1-vlan13@eth1 UP             192.168.0.10/30 fe80::55c8:670f:923b:962f/64 
eth1-vlan12@eth1 UP             192.168.0.1/30 fe80::1ee0:6d31:c35d:9cc/64 
```
#### Router3
```

lo               UNKNOWN        127.0.0.1/8 192.168.3.1/24 ::1/128 
eth0             UP             10.0.2.15/24 fe80::5054:ff:fe26:1060/64 
eth1             UP             
eth1-vlan13@eth1 UP             192.168.0.9/30 fe80::a2b9:bc2d:72ec:716a/64 
eth1-vlan23@eth1 UP             192.168.0.6/30 fe80::6052:88b1:d7c9:81f2/64
```
</p>
</details>

- конфиги /etc/quagga/*
- вывод tracepath для каждого из трёх случаев

<details><summary> OSPF поднят</summary>
<p>

#### Router1  
```
[root@Router1 ~]# tracepath 192.168.3.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.3.1 (192.168.3.1)                             0.457ms reached
 1:  192.168.3.1 (192.168.3.1)                             0.303ms reached
     Resume: pmtu 1500 hops 1 back 1 
[root@Router1 ~]# tracepath 192.168.2.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.2.1 (192.168.2.1)                             0.476ms reached
 1:  192.168.2.1 (192.168.2.1)                             0.427ms reached
     Resume: pmtu 1500 hops 1 back 1 
```
#### Router2
```
[root@Router2 ~]# tracepath 192.168.1.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.1.1 (192.168.1.1)                             0.585ms reached
 1:  192.168.1.1 (192.168.1.1)                             0.391ms reached
     Resume: pmtu 1500 hops 1 back 1 
[root@Router2 ~]# tracepath 192.168.3.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.3.1 (192.168.3.1)                             0.515ms reached
 1:  192.168.3.1 (192.168.3.1)                             0.473ms reached
     Resume: pmtu 1500 hops 1 back 1 
```
#### Router3
```
[root@Router3 ~]# tracepath 192.168.1.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.1.1 (192.168.1.1)                             0.568ms reached
 1:  192.168.1.1 (192.168.1.1)                             0.347ms reached
     Resume: pmtu 1500 hops 1 back 1 
[root@Router3 ~]# tracepath 192.168.2.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.2.1 (192.168.2.1)                             0.498ms reached
 1:  192.168.2.1 (192.168.2.1)                             0.436ms reached
     Resume: pmtu 1500 hops 1 back 1 
```
</p>
</details>

<details><summary> Изобразить ассиметричный роутинг</summary>
<p>

#### Router1  
```
[root@Router1 ~]# tracepath 192.168.3.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.0.2 (192.168.0.2)                             0.469ms 
 1:  192.168.0.2 (192.168.0.2)                             0.504ms 
 2:  192.168.3.1 (192.168.3.1)                             0.644ms reached
     Resume: pmtu 1500 hops 2 back 2 
```
#### Router3
```
[root@Router3 ~]# tracepath 192.168.1.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.1.1 (192.168.1.1)                             0.471ms reached
 1:  192.168.1.1 (192.168.1.1)                             0.424ms reached
     Resume: pmtu 1500 hops 1 back 1 
```
</p>
</details>

<details><summary> Сделать один из линков "дорогим", но что бы при этом роутинг был симметричным</summary>
<p>

#### Router1  

```
[root@Router1 ~]# tracepath 192.168.3.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.0.2 (192.168.0.2)                             0.461ms 
 1:  192.168.0.2 (192.168.0.2)                             0.438ms 
 2:  192.168.3.1 (192.168.3.1)                             0.737ms reached
     Resume: pmtu 1500 hops 2 back 2 
```

#### Router3 

```
[root@Router3 ~]# tracepath 192.168.1.1 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.0.5 (192.168.0.5)                             0.497ms 
 1:  192.168.0.5 (192.168.0.5)                             0.628ms 
 2:  192.168.1.1 (192.168.1.1)                             0.565ms reached
     Resume: pmtu 1500 hops 2 back 2 
```
</p>
</details>