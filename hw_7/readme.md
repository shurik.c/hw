**PAM**

*1. Запретить всем пользователям, кроме группы admin логин в выходные и праздничные дни*

Чтобы запретить пользователю логиниться в выходные дни можно воспользоваться модулем pam_time.so

Для этого необходимо:
* добавить строку account    required     pam_time.so в файле /etc/pam.d/login
* добавить строку login;*;user2;!Wk0000-2400 в файле /etc/security/time.conf

Для более тонкой настройки правил можно использовать модуль pam_script.so

Для его использования:
* Добавить строку auth required pam_script.so в файл /etc/pam.d/login
* Положить скрипт non_working_days_auth в /etc/pam-script.d/

*2. Дать конкретному пользователю права рута*


В PAM можно дать права включив в группу wheel через модуть pam_group.so

Для этого необходимо:

* добавить строку auth required pam_group.so файле /etc/pam.d/login
* добавить строку login;*;user1;Al0000-2400;wheel в файле /etc/security/group.conf

