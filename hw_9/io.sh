#!/bin/bash

time ionice -c 3 dd if=/dev/zero of=lio3.file bs=1M count=1024 && echo "Idle io complete!" &

time ionice -c 2 dd if=/dev/zero of=lio1.file bs=1M count=1024 && echo "Best-effort io complete!" &
