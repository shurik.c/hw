#!/bin/bash

CUR_PROCS=$(ls -1 /proc | grep ^[0-9] | sort -n)

trap 'echo "interrupted by user ...";exit 1' INT TERM

function ps_ax {

	echo " PID	TTY	STAT	TIME	COMMAND"

	for pid in $@
	do

		addrpid="/proc/$pid"
		if [[ -f "$addrpid/stat" ]];then

		# TTY

		PTTY=$(readlink $addrpid/fd/* | egrep "tty|pts" | head -n1 | sed 's*/dev/**')
		if [[ -z $PTTY ]]; then
			PTTY="?"
		fi

		# STAT

		PSTAT=`awk '{ORS="";
		if ( ARGIND == 1 ) {{print $3};
			if ( $19 > 0 ) {print "N"};
			if ( $6 == $1 ) {print "s"}
			};
		if ( ARGIND == 2 && / lo / ) {print "L"};
		if ( ARGIND == 1 ) {
			if ( $20 > 1 ) {print "l"};
			if ( $19 == -20 ) {print "<"};
			if ( $8 != -1 ) {print "+"}
			};
		}' $addrpid/stat $addrpid/smaps`

		# TIME

		PTIME=`awk '{print strftime("%M:%S", ($14+$15)/100)}' $addrpid/stat`

		# COMMAND

		CMMND=`cat $addrpid/cmdline | sed 's/\x0//g'`
		if [[ $CMMND == "" ]]; then
			CMMND=`awk '{print $2}' $addrpid/stat`
		fi

		echo "$pid	$PTTY	$PSTAT	$PTIME	$CMMND"
		fi
	done
}

ps_ax $CUR_PROCS
