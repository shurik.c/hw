
**Задания**

1) написать свою реализацию ps ax используя анализ /proc
- Результат ДЗ - рабочий скрипт который можно запустить

написан скрипт ps_ax.sh

3) дописать обработчики сигналов в прилагаемом скрипте, оттестировать, приложить сам скрипт, инструкции по использованию
- Результат ДЗ - рабочий скрипт который можно запустить + инструкция по использованию и лог консоли

в скрипт добавлен обработчик

    trap 'echo "interrupted by user ...";exit 1' INT TERM

4) реализовать 2 конкурирующих процесса по IO. пробовать запустить с разными ionice
- Результат ДЗ - скрипт запускающий 2 процесса с разными ionice, замеряющий время выполнения и лог консоли

        #!/bin/bash
    
        time ionice -c 3 dd if=/dev/zero of=lio3.file bs=1M count=1024 && echo "Idle io complete!" &
        time ionice -c 2 dd if=/dev/zero of=lio1.file bs=1M count=1024 && echo "Best-effort io complete!" &
        
        
        1024+0 records in
        1024+0 records out
        1073741824 bytes (1.1 GB, 1.0 GiB) copied, 20.9695 s, 51.2 MB/s
        
        real	0m21.201s
        user	0m0.001s
        sys	0m2.488s
        Best-effort io complete!
    
        1024+0 records in
        1024+0 records out
        1073741824 bytes (1.1 GB, 1.0 GiB) copied, 21.7943 s, 49.3 MB/s
        
        real	0m22.027s
        user	0m0.001s
        sys	0m2.504s
        Idle io complete!

По времени видно, что процесс, запущенный с ionice -c 3 завершился позже, но запущен был раньше

5) реализовать 2 конкурирующих процесса по CPU. пробовать запустить с разными nice
- Результат ДЗ - скрипт запускающий 2 процесса с разными nice и замеряющий время выполнения и лог консоли

        #!/bin/bash
        
        time nice -20 tar -czf ~/var_nice_20.tar.gz /var && echo "Nice +20 complete!" &
        
        time nice --19 tar -czf ~/var_nice__19.tar.gz /var && echo "Nice -19 complete!" &
        
        real	7m4.327s
        user	6m39.157s
        sys	0m24.856s
        Nice -19 complete!
        
        real	7m4.400s
        user	6m43.938s
        sys	0m21.815s
        Nice +20 complete!

Процесс, запущенный с высшим приоритетом (nice --19) завершился раньше