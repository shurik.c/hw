epel_repo
=========

Устанавливает epel репозиторий

Requirements
------------

CentOs/7

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: epel_repo }
