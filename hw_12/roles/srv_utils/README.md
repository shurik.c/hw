srv_utils
=========

Роль устанавливает пакеты для траблшутинга системы

Requirements
------------

CentOs и установленный репозиторий epel

Role Variables
--------------

srv_utils: - список устанавливаемых пакетов

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: srv_utils }
