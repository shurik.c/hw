tune_os
=========

Настраивает ядро для работы под нагрузкой.

Requirements
------------

Для работы роли требуется Systemd.

Role Variables
--------------

Переменные находятся в defaults\main.yml

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: tune_os }
