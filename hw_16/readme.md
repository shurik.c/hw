Домашнее задание
разворачиваем сетевую лабораторию

# otus-linux
Vagrantfile - для стенда урока 9 - Network

# Дано
https://github.com/erlong15/otus-linux/tree/network
(ветка network)

Vagrantfile с начальным построением сети
- inetRouter
- centralRouter
- centralServer

тестировалось на virtualbox

# Планируемая архитектура
построить следующую архитектуру

Сеть office1
- 192.168.2.0/26 - dev
- 192.168.2.64/26 - test servers
- 192.168.2.128/26 - managers
- 192.168.2.192/26 - office hardware

Сеть office2
- 192.168.1.0/25 - dev
- 192.168.1.128/26 - test servers
- 192.168.1.192/26 - office hardware


Сеть central
- 192.168.0.0/28 - directors
- 192.168.0.32/28 - office hardware
- 192.168.0.64/26 - wifi

```
Office1 ---\
-----> Central --IRouter --> internet
Office2----/
```
Итого должны получится следующие сервера
- inetRouter
- centralRouter
- office1Router
- office2Router
- centralServer
- office1Server
- office2Server

# Теоретическая часть
- Найти свободные подсети
- Посчитать сколько узлов в каждой подсети, включая свободные
- Указать broadcast адрес для каждой подсети
- проверить нет ли ошибок при разбиении

## Описание подсетей:

|Сеть office1 | broadcast | количество адресов|   
|---|---|---|
|- 192.168.2.0/26 - dev | 192.168.2.63| 62 |   
|- 192.168.2.64/26 - test servers | 192.168.2.127| 62 |   
|- 192.168.2.128/26 - managers | 192.168.2.191| 62 |   
|- 192.168.2.192/26 - office hardware | 192.168.2.255| 62 |   

|Сеть office2 | broadcast | количество адресов|
|---|---|---|
|- 192.168.1.0/25 - dev | 192.168.1.127 | 126 |   
|- 192.168.1.128/26 - test servers | 192.168.1.191 | 62 |   
|- 192.168.1.192/26 - office hardware | 192.168.1.255 | 62 |  

|Сеть central | broadcast | количество адресов|  
|---|---|---|
|- 192.168.0.0/28 - directors | 192.168.0.15 | 14 |
|- 192.168.0.16/28 – `free` | 192.168.0.31 | 14 |
|- 192.168.0.32/28 - office hardware | 192.168.0.47 | 14 |
|- 192.168.0.48/28 - `free` | 192.168.0.63 | 14 |
|- 192.168.0.64/26 - wifi | 192.168.0.127 | 62 |
|- 192.168.0.128/25 - `free` | 192.168.0.255 | 126 |

# Практическая часть
### Соединить офисы в сеть согласно схеме и настроить роутинг   

Включаем маршрутизацию на серверах:

    echo "net.ipv4.conf.all.forwarding=1" >> /etc/sysctl.conf
    sysctl -p

Настраиваем маршрутизацию:

    nmcli c mod "System eth1" +ipv4.routes "192.168.1.0/24 192.168.255.5"
    nmcli c mod "System eth1" +ipv4.routes "192.168.0.0/24 192.168.255.5"

### Все сервера и роутеры должны ходить в инет черз inetRouter

Добавляем шлюз маршрут:

    nmcli c mod "System eth1" ipv4.gateway "192.168.255.1"

```
[vagrant@office2Server ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=57 time=74.3 ms

[vagrant@office2Server ~]$ tracepath 8.8.8.8 -b
 1?: [LOCALHOST]                                         pmtu 1500
 1:  gateway (192.168.1.1)                                 1.171ms 
 1:  gateway (192.168.1.1)                                 1.319ms 
 2:  192.168.255.9 (192.168.255.9)                         1.896ms 
 3:  192.168.255.1 (192.168.255.1)                         2.901ms 
 4:  no reply
 ...
```

### Все сервера должны видеть друг друга
Проверяем:
```
[vagrant@office2Server ~]$ ping -c1 192.168.2.2
PING 192.168.2.2 (192.168.2.2) 56(84) bytes of data.
64 bytes from 192.168.2.2: icmp_seq=1 ttl=61 time=3.49 ms

--- 192.168.2.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.490/3.490/3.490/0.000 ms
[vagrant@office2Server ~]$ ping -c1 192.168.0.2
PING 192.168.0.2 (192.168.0.2) 56(84) bytes of data.
64 bytes from 192.168.0.2: icmp_seq=1 ttl=62 time=2.38 ms

--- 192.168.0.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 2.386/2.386/2.386/0.000 ms
[vagrant@office2Server ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=57 time=74.3 ms
```
### у всех новых серверов отключить дефолт на нат (eth0), который вагрант поднимает для связи

отключаем:

    echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
    systemctl restart network


### при нехватке сетевых интервейсов добавить по несколько адресов на интерфейс

добавляем, где надо:

    nmcli c mod "System eth1" +ipv4.address "192.168.255.5/30"
    nmcli c mod "System eth1" +ipv4.address "192.168.255.9/30"


