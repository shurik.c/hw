**LDAP**

*1. Установить FreeIPA*

Для сервера freeipa требуется установить следующие пакеты:

    ipa-server
    ipa-server-dns

перед запуском установки необходимо отредактировать файл /etc/hosts добавив в начало файла строку:

    192.168.11.110 ipasrv.testdomain.local ipasrv

установку можно запустить в интерактивном режиме или задать параметры:

    ipa-server-install -r TESTDOMAIN.LOCAL -n testdomain.local -p Passwd123 \
    -a Passwd123 --hostname=ipasrv.testdomain.local --ip-address=192.168.11.110 --no-forwarders --setup-dns -U

Для создания пользователей получаем тикет Kerberosa:

    echo "Passwd123" | kinit admin 

пользователя создать можно с помощью команды :

    ipa user-add vasilyok --first Vasiliy --last Pupkin --shell=/bin/bash --sshpubkey="$(cat id_rsa.pub)" ... etc.

или через веб интерфейс.

После этого можно подключаться к серверу по ключу:

    [vasilyok@ipacli .ssh]$ ssh vasilyok@ipasrv.testdomain.local 
    Last login: Thu Jan 10 22:29:55 2019
    [vasilyok@ipasrv ~]$ 

*2. Написать playbook для конфигурации клиента*

написаны плэйбуки для сервера и клиента

*3. Всю "сетевую лабораторию" перевести на аутентификацию через LDAP*

установить пакет:

    ipa-client

запустить команду:

    ipa-client-install --domain=testdomain.local --server=ipasrv.testdomain.local \
    -p admin -w Passwd123 --hostname=ipacli.testdomain.local --mkhomedir -U

*4. Настроить авторизацию по ssh-ключам*

пользователя создать можно с помощью команды :

    ipa user-add vasilyok --first Vasiliy --last Pupkin --shell=/bin/bash --sshpubkey="$(cat id_rsa.pub)" ... etc.

или через веб интерфейс.

После этого можно подключаться к серверу по ключу:

    [vasilyok@ipacli .ssh]$ ssh vasilyok@ipasrv.testdomain.local 
    Last login: Thu Jan 10 22:29:55 2019
    [vasilyok@ipasrv ~]$ 

