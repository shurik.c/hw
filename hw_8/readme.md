**1.Собрать пакет с параметрами**

Зависимости и репозиторий добавлены в Vagrantfile:

Создать пользователя от имени которого будет собираться пакет *(собирать пакеты от рута не желательно!)*

создать древо каталогов для сборки пакета можно с помощью `rpmdev-setuptree`

Скачиваем и распаковаываем исходники:

    yumdownloader --source nginx
    rpm -i nginx-*

Скачать openssl:

    wget https://www.openssl.org/source/openssl-1.1.1a.tar.gz -O rpmbuild/SOURCES/openssl-1.1.1a.tar.gz

Добавить в .spec файле строки:

    Source14: openssl-1.1.1a.tar.gz

    %setup -q -T -D -a 14
    
    --with-openssl=%{_builddir}/%{name}-%{main_version}/openssl-1.1.1a

Доустанавливаем зависимости для пакета:

    yum-builddep rpmbuild/SPECS/nginx.spec

Собираем пакет:

    rpmbuild -ba rpmbuild/SPECS/nginx.spec


**2.Создать репозиторий и добавить в него собранный пакет**

Установить собранный пакет:

    yum localinstall rpmbuild/RPMS/x86_64/nginx-* -y -q

Добавить секцию в конфиг nginx:

    location /repo {
        root   /opt;
        autoindex on;
    }

Создать директорию и скопировать в неё собраный пакет:

    mkdir -p /opt/repo
    cp rpmbuild/RPMS/x86_64/nginx-* /opt/repo

Создать репозиторий:

    createrepo /opt/repo/

Запустить nginx:

    systemctl start nginx


