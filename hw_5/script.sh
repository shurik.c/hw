#!/bin/bash

mail_address="root@localhost"						# e-mail
lock_file="/var/tmp/script_lock_file"				# multi-start protection
path_to_logs="/opt/script"							# where access & error files placed
file_inf="/opt/script/file.inf"						# last line 
script_log="/opt/script/my_script.log"				# log

check_lock () {
	if [ -f $lock_file ]
	then
		echo "Script in progress ..."
		exit 6
	fi
}

parse_params () {
	while getopts x:y: options
	do
		case "${options}"
			in
			x) ip_uniq=${OPTARG};;
			y) addr_ask=${OPTARG};;
			*) show_help;;
		esac
	done
}

show_help() {
	echo "Usage: script -x [ number of displayed IP ] -y [ number of asked resources ]"
	exit 6
	
}

parse_files () {
	if [ -z $ip_uniq ]; then
		ip_uniq=10
	fi
	if [ -z $addr_ask ]; then
		addr_ask=10
	fi

	result_ip=$(awk "( NR > $start_line_access )" $path_to_logs/access*.log | cut -d " " -f 1 | uniq -c | sort -nr | head -n $ip_uniq)
	result_ask=$(awk "( NR > $start_line_access )" $path_to_logs/access-*.log | cut -d '"' -f 4 | grep -v '^-' | sort | uniq -c | sort -nr | head -n $addr_ask)
	result_errors=$(awk "( NR > $start_line_error )" $path_to_logs/error*.log | awk '{ $1=$2=$3=$4=$5=""; print $0 }' | cut -d',' -f 1 | sort | uniq -c | sort -nr)
	result_codes=$(awk "( NR > $start_line_access )" $path_to_logs/access*.log | cut -d '"' -f 3 | awk '{ print $1 }' | sort | uniq -c | sort -nr)
	time_begin=$(awk "( NR > $start_line_access )" $path_to_logs/access*.log | head -n 1 | cut -d ' ' -f 4 | sed 's/\[//')
	time_end=$(awk "( NR > $start_line_access )" $path_to_logs/access*.log | tail -n 1 | cut -d ' ' -f 4 | sed 's/\[//')
	
	if [ "$start_line_access" -ne "$line_in_access" ]; then
		echo "Time range:
	[$time_begin - $time_end]

	Unique ip addreses:
	-----------------------------------------------------
${result_ip[*]}
	-----------------------------------------------------
	requested addresses
	-----------------------------------------------------
${result_ask[*]}
	-----------------------------------------------------
	Errors:
	-----------------------------------------------------
${result_errors[*]}
	-----------------------------------------------------
	Codes:
	-----------------------------------------------------
${result_codes[*]}" | mail -s "Script report $(date)" "$mail_address"
	else
		echo "No changes found" | mail -s "Script report $(date)" "$mail_address"
	fi
}

find_lines () {

	line_in_access=$(cat $path_to_logs/access*.log | wc -l)												# количество строк в файле access					line_in_access
	start_line_access=$(grep 'line_in_access' $file_inf | awk '{ print $2 }')							# номер строки в файле file.ini access				start_line_access
	line_in_error=$(cat $path_to_logs/error*.log | wc -l)												# количество строк в файле error					line_in_error
	start_line_error=$(grep 'line_in_error' $file_inf | awk '{ print $2 }')								# номер строки в файле file.ini error				start_line_error

		if [ "$start_line_access" -gt "$line_in_access" ]; then											#
			sed -i "s/line_in_access $start_line_access/line_in_access 0/" $file_inf
			start_line_access=0
		elif [ "$start_line_access" -lt "$line_in_access" ]; then
			sed -i "s/line_in_access $start_line_access/line_in_access $line_in_access/" $file_inf
		fi

		if [ "$start_line_error" -gt "$line_in_error" ]; then
			sed -i "s/line_in_error $start_line_error/line_in_error 0/" $file_inf
			start_line_error=0
		elif [ "$start_line_error" -lt "$line_in_error" ]; then
			sed -i "s/line_in_error $start_line_error/line_in_error $line_in_error/" $file_inf
		fi
}

main () {
	check_lock
	touch $lock_file
	trap 'rm -f "$lock_file"; exit 1' INT TERM EXIT
	parse_params $@
	find_lines >> $script_log 2>&1
	parse_files 2>> $script_log 
	rm -f $lock_file
}

if [ ! -f "$file_inf" ]; then
	echo "line_in_access 0" > $file_inf
	echo "line_in_error 0" >> $file_inf
fi

if [[ $1 = "-h" ]] || [[ $1 = "--help" ]];then
	show_help
else
	main $@
fi
