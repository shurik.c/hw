скрипт принимае аргументы -x [число]  -y [число]

-x	число уникальных ip
-y	число запрашиваемых адресов

внутри скрипта можно указать:

mail_address="root@localhost"						# адрес получателя
lock_file="/var/tmp/script_lock_file"				# файл для защиты от мультизапуска
path_to_logs="/opt/script"							# где access & error файлы лежат
file_inf="/opt/script/file.inf"						# файл для сохранени последней строки
script_log="/opt/script/my_script.log"				# ошибки скрипта
