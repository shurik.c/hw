# Домашнее задание
## Web сервера
##### Написать конфигурацию nginx, которая даёт доступ клиенту только с определенной cookie.
##### Если у клиента её нет, нужно выполнить редирект на location, в котором кука будет добавлена, после чего клиент будет обратно отправлен (редирект) на запрашиваемый ресурс.

Смысл: умные боты попадаются редко, тупые боты по редиректам с куками два раза не пойдут

Для выполнения ДЗ понадобятся
https://nginx.org/ru/docs/http/ngx_http_rewrite_module.html
https://nginx.org/ru/docs/http/ngx_http_headers_module.html

###### написана конфигурация: [nginx.conf](nginx.conf)

для двойного перенаправляния в конфигурацию нужно внести следующие изменения:
для корневой локации добавляем проверку, если кука отсутсвует то перенаправляем клиента в location /set_cookie кодом 301 (Moved Permanently)
```
        location / {
            if ($cookie_botstop != "yepp") {
            return 301 /set_cookie;
            }
        }
```
в location /set_cookie выдаём куку 'botstop=yepp' и отправляем обратно кодом 302 (Moved Temporarily)
```
        location /set_cookie {
            add_header Set-Cookie "botstop=yepp";
            return 302 /;
        }
    }
```

Проверяем:
```
[root@nginx ~]# curl -c "" -I -L 192.168.11.152
HTTP/1.1 301 Moved Permanently
Server: nginx/1.12.2
Date: Wed, 03 Apr 2019 14:36:55 GMT
Content-Type: text/html
Content-Length: 185
Location: http://192.168.11.152/set_cookie
Connection: keep-alive

HTTP/1.1 302 Moved Temporarily
Server: nginx/1.12.2
Date: Wed, 03 Apr 2019 14:36:55 GMT
Content-Type: text/html
Content-Length: 161
Location: http://192.168.11.152/
Connection: keep-alive
Set-Cookie: botstop=yepp

HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Wed, 03 Apr 2019 14:36:55 GMT
Content-Type: text/html
Content-Length: 3700
Last-Modified: Tue, 06 Mar 2018 09:26:21 GMT
Connection: keep-alive
ETag: "5a9e5ebd-e74"
Accept-Ranges: bytes
```